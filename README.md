This is a modified version of [Caffe](http://caffe.berkeleyvision.org). We add the double-margin contrastive loss layer and new IO functions for image-pairs input (see commits [55f3aaf](https://bitbucket.org/jonbakerfish/caffe/commits/55f3aaf12b4e78eaff6de81a6d2758244e6ad095) and [23b63fd](https://bitbucket.org/jonbakerfish/caffe/commits/23b63fd06f3276a703728126592ba1b4ca604f0a)).


If you use this code in your work, please cite our paper:

*Jiewei Cao, Zi Huang, Peng Wang, Chao Li, Xiaoshuai Sun, and Heng Tao Shen. [Quartet-net Learning for Visual Instance Retrieval](https://sites.google.com/site/quartetnetlearning/). In ACM Multimedia, 2016.*


**CAUTION**: This work was once submitted to ICCV on 2015-04-22 and the code is based on the Caffe version at that time. We will migrate to the latest Caffe in the future

**UPDATE 2016-07-17**: [A PR on GitHub for the latest version](https://github.com/BVLC/caffe/pull/4476) is created.
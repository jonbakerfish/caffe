#include <glog/logging.h>
#include <leveldb/db.h>
#include <lmdb.h>
#include <stdint.h>

#include <algorithm>
#include <string>

#include "caffe/proto/caffe.pb.h"
#include "caffe/util/io.hpp"

using caffe::Datum;
using caffe::BlobProto;
using std::string;
using std::max;

int main(int argc, char** argv) {
  // ::google::InitGoogleLogging(argv[0]);
  string in_mean_file = "/home/uqjcao4/Code/caffe/"
        "data/ilsvrc12/imagenet_mean.binaryproto";
  string out_mean_file = "/home/uqjcao4/Code/caffe/"
        "data/ilsvrc12/paired_imagenet_mean.binaryproto";

  BlobProto sum_blob;
  BlobProto sum_blob2;
  if (!ReadProtoFromBinaryFile(in_mean_file.c_str(), &sum_blob)) {
    LOG(INFO) << "Can't open:" << in_mean_file;
    return 0;
  }

  sum_blob2.set_num(1);
  sum_blob2.set_channels(sum_blob.channels()*2);
  sum_blob2.set_height(sum_blob.height());
  sum_blob2.set_width(sum_blob.width());
  const int data_1_size = sum_blob.data_size();
  for (int i = 0; i < data_1_size * 2; ++i) {
    sum_blob2.add_data(0.);
  }

  for (int i = 0; i < data_1_size; ++i) {
    sum_blob2.set_data(i, sum_blob.data(i));
    sum_blob2.set_data(i + data_1_size, sum_blob.data(i));
  }

  // Write to disk
  LOG(INFO) << "Write to " << out_mean_file;
  WriteProtoToBinaryFile(sum_blob2, out_mean_file.c_str());

  return 0;
}
